﻿namespace InterviewTaskAE.Models.Contracts
{
    /// <summary>
    /// Interface IEntity.
    /// An interface defining the base properties of all models. />
    /// </summary>
    public interface IEntity
    {
        public int Id { get; }

        public string Name { get; }
        public int? ApiId { get; }
    }
}