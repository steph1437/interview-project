﻿using InterviewTaskAE.Models.Abstract;
using System.ComponentModel.DataAnnotations;

namespace InterviewTaskAE.Models
{
    public class Artwork : Entity
    {
        [Required]
        public int? ArtistId { get; set; }
        public Artist Artist { get; set; }
        public string Description { get; set; }
        public string PlaceOfOrigin { get; set; }
        public string AssumedDate { get; set; } 
        public string Dimensions { get; set; }
        public string Medium { get; set; }
        public string Url { get; set; }
    }
}
