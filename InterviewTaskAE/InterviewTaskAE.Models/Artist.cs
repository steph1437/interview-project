﻿using InterviewTaskAE.Models.Abstract;
using System.Collections.Generic;

namespace InterviewTaskAE.Models
{
    public class Artist : Entity
    {
        public ICollection<Artwork> Artworks { get; set; }
    }
}
