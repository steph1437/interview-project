﻿using InterviewTaskAE.Models.Contracts;

namespace InterviewTaskAE.Models.Abstract
{
    public abstract class Entity : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ApiId { get; set; }
    }
}
