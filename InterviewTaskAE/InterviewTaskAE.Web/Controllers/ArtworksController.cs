﻿using DinkToPdf;
using DinkToPdf.Contracts;
using InterviewTaskAE.Services.Contracts;
using InterviewTaskAE.Web.Utility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace InterviewTaskAE.Web.Controllers
{
    public class ArtworksController : Controller
    {
        private readonly ILogger<ArtworksController> _logger;
        private readonly IArticAPIService _apiService;
        private readonly IConverter _converter;

        public ArtworksController(ILogger<ArtworksController> logger, IArticAPIService apiService, IConverter converter)
        {
            _converter = converter;
            _logger = logger;
            _apiService = apiService;
        }

        public async Task<IActionResult> Index(int page)
        {
            try
            {
                var artworks = await _apiService.ExtractArtworksAsync(page);
                return View(artworks);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var artwork = await _apiService.ExtractArtworkByIdAsync(id);
                return View(artwork);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public async Task<IActionResult> Search(string search, int page)
        {
            try
            {
                ViewBag.Search = search;
                var artworks = await _apiService.SearchArtworksAsync(search, page);
                artworks.Pagination.SearchCriteria = search;
                return View(artworks);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public async Task<IActionResult> CreatePdf(int id)
        {
            try
            {
                var artwork = await _apiService.ExtractArtworkByIdAsync(id);

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report"
                };
                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = TemplateGenerator.GetHTMLString(artwork),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/css", "styles.css") }
                };

                var pdf = new HtmlToPdfDocument
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);

                return File(file, "application/pdf", "PDFReport.pdf");
            }
            catch (Exception)
            {
                return View("Error");
            }
        }
    }
}
