﻿using InterviewTaskAE.Models;
using System.Text;

namespace InterviewTaskAE.Web.Utility
{
    public static class TemplateGenerator
    {
        public static string GetHTMLString(Artwork artwork)
        {

            var sb = new StringBuilder();
            sb.AppendFormat(@"
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='header'><h1>{0}</h1></div>
                                <table align='center'>
                                <img src='{1}' alt='Girl in a jacket' class='center' width='450' height='550'>
                                    <table align='center'>
                                    <tr>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>Artist</th>
                                        <th>Origin</th>
                                    </tr>", artwork.Name, artwork.Url);

            sb.AppendFormat(@"<tr>
                                    <td>{0}</td>
                                    <td>{1}</td>
                                    <td>{2}</td>
                                    <td>{3}</td>
                                  </tr>", artwork.AssumedDate, artwork.Description, artwork.Artist.Name, artwork.PlaceOfOrigin);
        
            sb.Append(@"

                            </body>
                        </html>");

            return sb.ToString();
        }
    }
}