﻿using InterviewTaskAE.Services.Contracts;
using System.Net.Http;
using System.Threading.Tasks;

namespace InterviewTaskAE.Services
{
    public class HttpArticClientService : IHttpArticClientService
    {
        private readonly HttpClient _httpClient;

        public HttpArticClientService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<HttpResponseMessage> GetAsync(string uri)
            => await _httpClient.GetAsync(uri);
    }
}
