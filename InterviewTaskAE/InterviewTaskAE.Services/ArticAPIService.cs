﻿using InterviewTaskAE.Models;
using InterviewTaskAE.Services.Contracts;
using InterviewTaskAE.Services.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewTaskAE.Services
{
    public class ArticAPIService : IArticAPIService
    {
        private readonly IHttpArticClientService _httpClient;
        public ArticAPIService(IHttpArticClientService httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<ListingModel> ExtractArtworksAsync(int page)
        {
            string url = "https://api.artic.edu/api/v1/artworks?page=" + page;
            var artworksResponse = await _httpClient.GetAsync(url);
            var artworksResponseResult = await artworksResponse.Content.ReadAsStringAsync();
            var artworks = JsonConvert.DeserializeObject<ArtworksAPIModel>(artworksResponseResult);
            var artworksToDisplay = new ListingModel
            {
                Pagination = artworks.pagination,
                Artworks = new List<Artwork>()
            };
            foreach (var artwork in artworks.Data)
            {
                artworksToDisplay.Artworks.Add(CreateArtwork(artwork));
            }
            return artworksToDisplay;
        }

        public async Task<Artwork> ExtractArtworkByIdAsync(int id)
        {
            string url = "https://api.artic.edu/api/v1/artworks/" + id;
            var artworkResponse = await _httpClient.GetAsync(url);
            var artworkResponseResult = await artworkResponse.Content.ReadAsStringAsync();
            var artworkToDisplay = JsonConvert.DeserializeObject<ArtworkAPIModel>(artworkResponseResult);

            return CreateArtwork(artworkToDisplay.Data);
        }

        public async Task<ListingModel> SearchArtworksAsync(string searchstring, int page)
        {
            string url = "https://api.artic.edu/api/v1/artworks/search?q=" + searchstring + "&page=" + page;
            var artworksResponse = await _httpClient.GetAsync(url);
            var artworksResponseResult = await artworksResponse.Content.ReadAsStringAsync();
            var artworks = JsonConvert.DeserializeObject<ArtworksAPIModel>(artworksResponseResult);
            var artworksToDisplay = new ListingModel
            {
                Pagination = artworks.pagination,
                Artworks = new List<Artwork>()
            };
            foreach (var artwork in artworks.Data)
            {
                var artworkToAdd = await ExtractArtworkByIdAsync(artwork.Id);
                artworksToDisplay.Artworks.Add(artworkToAdd);
            }

            return artworksToDisplay;
        }

        /// <summary>
        /// Creates an artwork. The Method is probably unneeded if a database isn't built.
        /// </summary>
        /// <param name="artwork">The artwork's data.</param>
        private Artwork CreateArtwork(Models.Data artwork)
        {
            ValidateModel(artwork);
            if (artwork.ArtistName == null)
                artwork.ArtistName = "Unknown";
            var addedArtist = new Artist
            {
                ApiId = artwork.ArtistId,
                Name = artwork.ArtistName
            };

            var addedArtwork = new Artwork
            {
                ApiId = artwork.Id,
                Name = artwork.Title,
                ArtistId = artwork.ArtistId,
                Artist = addedArtist,
                Description = artwork.Description.DescriptionText,
                PlaceOfOrigin = artwork.PlaceOfOrigin,
                AssumedDate = artwork.AssumedDate,
                Dimensions = artwork.Dimensions,
                Medium = artwork.Medium,
                Url = artwork.ImageId
            };

            return addedArtwork;
        }

        /// <summary>
        /// Validates an artwork, and changes its parameters if needed..
        /// </summary>
        /// <param name="artwork">The artwork's data.</param>
        private void ValidateModel(Models.Data artwork)
        {
            if (artwork.Title == null)
                artwork.Title = "Title unavailable.";

            if (artwork.Description == null)
                artwork.Description = new Thumbnail { DescriptionText = "Description unavailable." };

            if (artwork.PlaceOfOrigin == null)
                artwork.PlaceOfOrigin = "Place of Origin unavailable.";

            if (artwork.AssumedDate == null)
                artwork.AssumedDate = "Assumed date of creation unavailable.";

            if (artwork.Dimensions == null)
                artwork.Dimensions = "Dimensions unavailable.";

            if (artwork.Medium == null)
                artwork.Medium = "Medium unavailable";

            if (artwork.ImageId == null)
                artwork.ImageId = "https://scontent.fsof11-1.fna.fbcdn.net/v/t1.6435-9/128851635_5427040923980029_649285277980215436_n.png?_nc_cat=102&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=Y5ulQ0sEmMEAX-qNf5H&_nc_ht=scontent.fsof11-1.fna&oh=bb3b8c86e3349cdde28a214ffb4020de&oe=60EC7245";
            else
                artwork.ImageId = "https://www.artic.edu/iiif/2/" + $"{artwork.ImageId}" + "/full/843,/0/default.jpg";
        }
    }
}
