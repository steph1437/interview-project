﻿using InterviewTaskAE.Models;
using System.Collections.Generic;

namespace InterviewTaskAE.Services.Models
{
    public class ListingModel
    {
        public List<Artwork> Artworks { get; set; }
        public Pagination Pagination { get; set; }
    }
}
