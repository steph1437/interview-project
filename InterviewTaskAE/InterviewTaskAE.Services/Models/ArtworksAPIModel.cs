﻿namespace InterviewTaskAE.Services.Models
{
    public class ArtworksAPIModel
    {
        public Pagination pagination { get; set; }
        public Data[] Data { get; set; }
    }
}
