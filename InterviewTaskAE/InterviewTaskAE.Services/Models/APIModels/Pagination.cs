﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace InterviewTaskAE.Services.Models
{
    public class Pagination
    {
        [JsonProperty("total_pages")]
        public int TotalPages { get; set; }
        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }
        public string SearchCriteria { get; set; }
    }
}
