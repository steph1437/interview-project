﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace InterviewTaskAE.Services.Models
{
    public class Data
    {      
        public int Id { get; set; }  

        public string Title { get; set; }

        [JsonProperty("thumbnail")]
        public Thumbnail Description { get; set; }

        [JsonProperty("date_display")]
        public string AssumedDate { get; set; }

        [JsonProperty("place_of_origin")]
        public string PlaceOfOrigin { get; set; }

        public string Dimensions { get; set; }

        [JsonProperty("medium_display")]
        public string Medium { get; set; }

        [JsonProperty("artist_id")]
        public int? ArtistId { get; set; }

        [JsonProperty("artist_title")]
        public string ArtistName { get; set; }

        [JsonProperty("image_id")]
        public string ImageId { get; set; }
    }
}
