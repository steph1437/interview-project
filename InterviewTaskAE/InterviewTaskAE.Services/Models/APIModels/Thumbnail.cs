﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace InterviewTaskAE.Services.Models
{
    public class Thumbnail
    {
        [JsonProperty("alt_text")]
        public string DescriptionText { get; set; }
    }
}
