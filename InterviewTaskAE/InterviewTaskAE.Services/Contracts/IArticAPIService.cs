﻿using InterviewTaskAE.Models;
using InterviewTaskAE.Services.Models;
using System.Threading.Tasks;

namespace InterviewTaskAE.Services.Contracts
{
    /// <summary>
    /// API Interface containing all necesseray services to list artworks.
    /// </summary>
    public interface IArticAPIService
    {
        /// <summary>
        /// Extracts all artworks from a base listing query.
        /// </summary>
        /// <param name="page">The page number of the query.</param>
        Task<ListingModel> ExtractArtworksAsync(int page);
        /// <summary>
        /// Extract an artwork by its Id.
        /// </summary>
        /// /// <param name="id">Artwork's id.</param>
        Task<Artwork> ExtractArtworkByIdAsync(int id);
        /// <summary>
        /// Extract artworks by a search criteria.
        /// </summary>
        /// <param name="searchcriteria">The searchcriteria.</param>
        /// <param name="page">The page number.</param>
        Task<ListingModel> SearchArtworksAsync(string searchcriteria, int page);
    }
}
