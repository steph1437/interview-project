﻿using System.Net.Http;
using System.Threading.Tasks;

namespace InterviewTaskAE.Services.Contracts
{
    public interface IHttpArticClientService
    {
        Task<HttpResponseMessage> GetAsync(string url);
    }
}
