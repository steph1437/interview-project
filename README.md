![alt](https://i.ibb.co/C7G2xrp/articlogo.png)

---


# Interview Project
> My trello can be accessed under https://trello.com/b/eHku2cBX/interview-project

> The project utilises the api of the Art Institute of Chicago.
The project is started through InterviewTaskAE.Web running on https://localhost:5001/

> I first started doing the project with creating an EF Database, but after that I decided to do the project without a local database, as the API is getting updated every couple of minutes or so,
and I think it would make no sense to create a database and still run requests to the API, so I can check if the data has been updated.

> The artworks button lists Artworks under the https://api.artic.edu/api/v1/artworks/ endpoint and uses its default pagination.
When accessing the details of an artwork you get the option to download a PDF file with its details.

> The search function utilises the https://api.artic.edu/api/v1/artworks/search/ endpoint. As it doesn't actually fully lists the artwork and its details, but rather only its Id, I'm doing an https://api.artic.edu/api/v1/artworks/{id} request on each artwork listed. That makes it quite a bit slower, but that is the only way to get the details of the artworks, so I can show the their pictures and information.

> The search function utilises the https://api.artic.edu/api/v1/artworks/search/ endpoint. As it doesn't actually fully lists the artwork and its details, but rather only its Id, I'm doing an https://api.artic.edu/api/v1/artworks/{id} request on each artwork listed. That makes it quite a bit slower, but that is the only way to get the details of the artworks, so I can show the their pictures and information.

> The PDF-Generation utilises the DinkToPdf, I created a HTML page and saved it into a stringbuilder, so I can run convert the HTML to a PDF file.
